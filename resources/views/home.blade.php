<!DOCTYPE html>
<html>
<head>
    <title>Homepage</title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>

<div class="container">
    <br><br>
    <center>
        <h2>Sample posts</h2>
    </center>
    <br><br>
    <div class="row">
        @foreach($posts as $post)
            <div class="col-md-6" style="padding-bottom: 30px; ">
                <div>
                    <a href="/post/{{ $post->slug }}">
                        <img src="{{ Voyager::image( $post->image ) }}" style="width:100%; border-radius: 10px;">
                        <h3>{{ $post->title }}</h3>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>

</body>
</html>
